$(function(){

  $('.js-header-dropdown').click(function(e){
      $('.header-dropdown').toggle()
      e.preventDefault()
  })

  $('.authorization-tabs__link').click(function(e){
    $('.authorization-tabs__link').removeClass('active')
    $('.authorization-tabblock').removeClass('active')
    $(this).addClass('active')
    var id = $(this).attr('href')
    $(id).addClass('active')
    e.preventDefault()
  })

  $('.item-btn').on('click', function(){
    $(this).parents('.item').find('.item-block__dropdown').slideToggle()
  })

})

function readURL(input) {
  if (input.files && input.files[0]) {
    
    var fileTypes = ['jpg', 'jpeg', 'png'],
        reader    = new FileReader(),
        preview   = $(input).parents('.upload').find('.upload-preview img'),
        extension = input.files[0].name.split('.').pop().toLowerCase(),
        isSuccess = fileTypes.indexOf(extension) > -1;

    if (input.files && input.files[0]) {      

      if (isSuccess) {
        var reader = new FileReader();
        reader.onload = function (e) {          
          preview.attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
      else { 
        alert('Можно загружать только картинки jpg/png')
      }
    }
   
  }
}